# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################


{
    'name': 'laskurivi',
    'version': '0.1',
    'license': 'Other proprietary',
    'category': 'll',
    'description': 'll',
    'author': 'Sprintit ltd',
    'maintainer': 'Sprintit ltd',
    'website': 'http://www.sprintit.fi',
    'depends': [
      'base', 
      'sale',
    ],
    'data': [
      'view/sale_order_view.xml',
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

