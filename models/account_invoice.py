# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################



from odoo import models, fields, api, _

class AccountInvoiceLine(models.Model):
    bruttohinta = fields.Many2one('sale.order.line' , string='Bruttohinta')
    related_bruttohinta = fields.Float('Bruttohinta', related='bruttohinta.bruttohinta')
    
    